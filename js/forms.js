$().ready(function () {
    $('.form-group.floating-label .form-control').on('input', function (e) {
        if (e.target.value) {
            $(this).parent().addClass('has-value')
        } else {
            $(this).parent().removeClass('has-value')
        }
    })

    $('.form-group.password-shown button').on('click', function () {
        const passwordType = $(this).parent().find('input').attr('type')
        if (passwordType === 'password') {
            $(this).parent().find('input').attr('type', 'text')
            $(this).parent().find('i').removeClass('fa-eye')
            $(this).parent().find('i').addClass('fa-eye-slash')
        } else {
            $(this).parent().find('input').attr('type', 'password')
            $(this).parent().find('i').addClass('fa-eye')
            $(this).parent().find('i').removeClass('fa-eye-slash')
        }

    })

    $('.cep-mask').inputmask({mask: '99.999-99', showMaskOnHover: false, showMaskOnFocus: false})
    $('.cpf-mask').inputmask({mask: '999.999.999-99', showMaskOnHover: false, showMaskOnFocus: false})
    $('.cpf-cnpj-mask').inputmask({mask: ['999.999.999-99', '99.999.999/9999-99'], showMaskOnHover: false, showMaskOnFocus: false})
    $('.tel-mask').inputmask({mask: '(99) 9999[9]-9999', showMaskOnHover: false, showMaskOnFocus: false})
    $('.date-mask').inputmask({mask: '99/99/9999', showMaskOnHover: false, showMaskOnFocus: false})
    $('.time-mask').inputmask({mask: '99:99', showMaskOnHover: false, showMaskOnFocus: false})
    $('.card-mask').inputmask({mask: '9999 9999 9999 9999', showMaskOnHover: false, showMaskOnFocus: false})
    $('.val-mask').inputmask({mask: '99/99', showMaskOnHover: false, showMaskOnFocus: false})
    $('.cvv-mask').inputmask({mask: '999', showMaskOnHover: false, showMaskOnFocus: false})
    $('.currency-mask').maskMoney({prefix: 'R$ ', thousands: '.', decimal: ',', allowEmpty: true, allowZero: true});
    $('.km-mask').maskMoney({suffix: ' km', thousands: '.', decimal: ',', precision: 0, allowEmpty: true, allowZero: true});
})
