$().ready(function () {
    const chat = $('#chat')
    chat.on('click', function (e) {
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened')
        } else {
            $(this).addClass('opened')
        }
    })
    scrollEvt()
    document.addEventListener('scroll', function () {
        scrollEvt()
    })
})


function scrollEvt () {
    const chat = $('#chat')
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    const scrollHeight = doc.scrollHeight - doc.offsetHeight
    const atFooter = top > scrollHeight - 71
    if (atFooter) {
        chat.css('bottom', Math.abs(scrollHeight - 71 - top) + 'px')
    } else {
        chat.css('bottom', 0 + 'px')
    }
}
