$().ready(function () {
    $('[data-dropdown]').on('click', function (e) {
        e.preventDefault()
        if ($(this).find('.dropdown-menu').hasClass('opened')) {
            $(this).find('.dropdown-menu').removeClass('opened')
            $(this).find('.dropdown-menu').hide()
        } else {
            $(this).find('.dropdown-menu').addClass('opened')
            $(this).find('.dropdown-menu').show()
        }
    })

    document.addEventListener('click', function (e) {
        var drop = $(e.target).parents('[data-dropdown]')
        if(drop.length === 0) {
            $('[data-dropdown] .dropdown-menu').removeClass('opened')
            $('[data-dropdown] .dropdown-menu').hide()
        }
    })

    // $('').on('click', function (e) {
    //     const drop = $(this).parents('[data-dropdown]')
    //     if(drop.length === 0) {
    //         console.log('askdfj')
    //         // $('[data-dropdown] .dropdown-menu').removeClass('opened')
    //         // $('[data-dropdown] .dropdown-menu').hide()
    //     }
    // })
})
